<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class LaporanRespondenSurveyExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents
{
    public function query()
    {
        $data = DB::table('jawaban as j')
        ->select(DB::raw('s.nama_satker as satker, count(j.satker_id) as jumlah'))
        ->rightJoin('satkers as s', 's.id', '=', 'j.satker_id')
        ->leftJoin('jawaban_detail as jd', 'j.id', '=', 'jd.id_jawaban');

        $data->groupBy('j.satker_id');
        $data->groupBy('s.nama_satker');
        $data->groupBy('s.id');
        $data->orderByDesc(DB::raw('COUNT(j.satker_id)'));

        return $data->orderBy('s.id');
    }

    public function headings(): array
    {
        return [
            'Nama Satker',
            'Jumlah',
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:B1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ]
                    ]
                );
            },
        ];
    }
}
