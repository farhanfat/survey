<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class LaporanHasilSurveyExport implements FromQuery, WithHeadings, ShouldAutoSize, WithEvents
{

    // public function __construct($startDatePurchaseService, $endDatePurchaseService, $spkNumberPurchaseService)
    // {
    //     $this->startDatePurchaseService = $startDatePurchaseService;
    //     $this->endDatePurchaseService = $endDatePurchaseService;
    //     $this->spkNumberPurchaseService = $spkNumberPurchaseService;
    // }

    public function query()
    {
        $data = DB::table('satkers as s')
        ->select(DB::raw('s.nama_satker as nama_satker,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'sangat_kurang\' GROUP BY jawaban_detail.jawaban) as sangat_kurang,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'kurang\' GROUP BY jawaban_detail.jawaban) as kurang,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'cukup\' GROUP BY jawaban_detail.jawaban) as cukup,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'baik\' GROUP BY jawaban_detail.jawaban) as baik,
        (select count(jawaban_detail.jawaban) from jawaban_detail join jawaban on jawaban.id = jawaban_detail.id_jawaban join satkers on satkers.id = jawaban.satker_id where satkers.nama_satker = s.nama_satker and  jawaban_detail.jawaban = \'sangat_baik\' GROUP BY jawaban_detail.jawaban) as sangat_baik
        '))
        ->leftJoin('jawaban as j', 's.id', '=', 'j.satker_id')
        ->leftJoin('jawaban_detail as jd', 'j.id', '=', 'jd.id_jawaban');
        $data->groupBy('s.nama_satker');

        return $data->groupBy('s.id')->orderBy('s.id', 'asc');
    }

    public function headings(): array
    {
        return [
            'Nama Satker',
            'Sangat Kurang',
            'Kurang',
            'Cukup',
            'Baik',
            'Sangat Baik',
        ];
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:F1',
                    [
                        'font' => [
                            'name'  => 'Calibri',
                            'size'  =>  12,
                            'bold'  => 'true'
                        ]
                    ]
                );
            },
        ];
    }
}
