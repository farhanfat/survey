<?php

namespace App\Http\Livewire\Table;

use Livewire\Component;
use Livewire\WithPagination;

class Main extends Component
{
    use WithPagination;

    public $model;
    public $name;

    public $perPage = 10;
    public $sortField = "id";
    public $sortAsc = true;
    public $search = '';
    public $dateStart;
    public $dateEnd;

    protected $listeners = [ "deleteItem" => "delete_item" ];

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function get_pagination_data ()
    {
        switch ($this->name) {
            case 'user':
                $users = $this->model::search($this->search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);

                return [
                    "view" => 'livewire.table.user',
                    "users" => $users,
                    "data" => array_to_object([
                        'href' => [
                            'create_new' => route('user.new'),
                            'create_new_text' => 'Buat User Baru',
                            // 'export' => '#',
                            // 'export_text' => 'Export'
                        ]
                    ])
                ];
            break;
            case 'satker':
                $satker = $this->model::search($this->search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);

                return [
                    "view" => 'livewire.table.satker',
                    "satker" => $satker,
                    "data" => array_to_object([
                        'href' => [
                            'create_new' => route('satker.new'),
                            'create_new_text' => 'Buat Satker Baru',
                            // 'export' => '#',
                            // 'export_text' => 'Export'
                        ]
                    ])
                ];
            break;
            case 'daftarhasil':
                $satker = $this->model::search($this->search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);

                return [
                    "view" => 'livewire.table.daftarhasil',
                    "satker" => $satker,
                    "data" => array_to_object([
                        'href' => [
                            'create_new' => '#',
                            'create_new_text' => '',
                            // 'export' => '#',
                            // 'export_text' => 'Export'
                        ]
                    ])
                ];
            break;
            case 'pertanyaan':
                $pertanyaan = $this->model::search($this->search)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);

                return [
                    "view" => 'livewire.table.pertanyaan',
                    "pertanyaan" => $pertanyaan,
                    "data" => array_to_object([
                        'href' => [
                            'create_new' => route('pertanyaan.new'),
                            'create_new_text' => 'Buat Pertanyaan Baru',
                            // 'export' => '#',
                            // 'export_text' => 'Export'
                        ]
                    ])
                ];
            break;
            case 'laporanhasilsurvey':
                $laporanhasilsurvey = $this->model::search($this->search, $this->dateStart, $this->dateEnd)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);


                return [
                    "view" => 'livewire.table.laporanhasilsurvey',
                    "laporanhasilsurvey" => $laporanhasilsurvey,
                    "data" => array_to_object([
                        'href' => [
                            // 'create_new' => '#',
                            // 'create_new_text' => '',
                            'export' => route('hasilsurvey.exportExcel'),
                            'export_text' => 'Export',
                            'filter_date' => 'true',
                        ]
                    ])
                ];
            break;
            case 'laporanrespondensurvey':
                $laporanrespondensurvey = $this->model::search($this->search, $this->dateStart, $this->dateEnd)
                    ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                    ->paginate($this->perPage);


                return [
                    "view" => 'livewire.table.laporanrespondensurvey',
                    "laporanrespondensurvey" => $laporanrespondensurvey,
                    "data" => array_to_object([
                        'href' => [
                            // 'create_new' => '#',
                            // 'create_new_text' => '',
                            'export' => route('respondensurvey.exportExcel'),
                            'export_text' => 'Export',
                            'filter_date' => 'true',
                        ]
                    ])
                ];
            break;
            default:
                # code...
                break;
        }
    }

    public function delete_item ($id)
    {
        $data = $this->model::find($id);

        if (!$data) {
            $this->emit("deleteResult", [
                "status" => false,
                "message" => "Gagal menghapus data " . $this->name
            ]);
            return;
        }

        $data->delete();
        $this->emit("deleteResult", [
            "status" => true,
            "message" => "Data " . $this->name . " berhasil dihapus!"
        ]);
    }

    public function render()
    {
        $data = $this->get_pagination_data();

        return view($data['view'], $data);
    }
}
