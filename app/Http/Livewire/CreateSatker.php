<?php

namespace App\Http\Livewire;

use App\Models\Satker;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class CreateSatker extends Component
{
    public $satker;
    public $satkerId;
    public $action;
    public $button;
    public $comboSatker;

    protected function getRules()
    {
        $rules = ($this->action == "updateSatker") ? [
            'satker.nama_satker' => 'required' 
        ] : [
            'satker.parent_satker' => 'required'
        ];

        return array_merge([
            'satker.nama_satker' => 'required|min:3',
            'satker.parent_satker' => 'required|min:1'
        ], $rules);
    }

    public function getComboSatker() {
        $data = DB::table('satkers')
            ->select("id", "nama_satker");
        $result = $data->get();
        $this->comboSatker = $result;
    }

    public function createSatker ()
    {
        $this->resetErrorBag();
        $this->validate();

        Satker::create($this->satker);

        $this->emit('saved');
        $this->reset('satker');
        $this->getComboSatker();
    }

    public function updateSatker ()
    {
        $this->resetErrorBag();
        $this->validate();

        Satker::query()
            ->where('id', $this->satkerId)
            ->update([
                "nama_satker" => $this->satker->nama_satker,
                "parent_satker" => $this->satker->parent_satker,
            ]);

        $this->emit('saved');
        $this->getComboSatker();
    }

    public function mount ()
    {
        if (!$this->satker && $this->satkerId) {
            $this->satker = Satker::find($this->satkerId);
        }

        $this->button = create_button($this->action, "Satker");
        $this->getComboSatker();
    }

    public function render()
    {
        return view('livewire.create-satker');
    }
}
