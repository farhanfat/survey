<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Jawaban;
use Illuminate\Http\Request;

class JawabanController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 5);
        $jawaban_dari = $request->input('jawaban_dari');
        $satker_id = $request->input('satker_id');
        $pertanyaan_id = $request->input('pertanyaan_id');
        $jawaban = $request->input('jawaban');
        
        if($id)
        {
            $jawaban = Jawaban::find($id);

            if($jawaban)
            {
                return ResponseFormatter::success(
                    $jawaban,
                    'Data Jawaban Berhasil Diambil'
                );
            }
            else
            {
                return ResponseFormatter::error(
                    null,
                    'Data Jawaban Tidak Ada',
                    404
                );
            }
        }

        $jawaban = Jawaban::query();

        // if($jawaban_dari)
        // {
        //     $jawaban->where('jawaban_dari','like','%'. $jawaban_dari . '%');
        // }

        // if($satker_id)
        // {
        //     $jawaban->where('satker_id','=', $satker_id);
        // }

        // if($pertanyaan_id)
        // {
        //     $jawaban->where('pertanyaan_id','=', $pertanyaan_id);
        // }

        // if($jawaban)
        // {
        //     $jawaban->where('jawaban','=', $jawaban);
        // }


        return ResponseFormatter::success(
            $jawaban->paginate($limit),
            'Data Jawaban Berhasil Diambil'
        );
    }

    public function tambah(Request $request)
    {
        try {
            $request->validate([
                'pertanyaan_id' => ['required','integer','max:10'],
                'satker_id' => ['required','integer','max:10'],
                'jawaban_dari' => ['required','string','max:255'],
                'jawaban' => ['required','string','max:255'],
            ]);

            Jawaban::create([
                'jawaban_dari' => $request->jawaban_dari,
                'satker_id' => $request->satker_id,
                'pertanyaan_id' => $request->pertanyaan_id,
                'jawaban' => $request->jawaban,
            ]);
            $jawaban = jawaban::where('jawaban_dari', $request->jawaban_dari)->first();
            return ResponseFormatter::success([
                'jawaban' => $jawaban
            ]);
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong cuyy',
                'error' => $error
            ], 'Tambah Data Gagal', 500);
        }
    }
}
