<?php

namespace App\Http\Controllers\API;
use App\Actions\Fortify\PasswordValidationRules;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use toke;

class UserController extends Controller
{

    use PasswordValidationRules;
    
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 5);
        $username = $request->input('username');
        $name = $request->input('name');
        $email = $request->input('email');
        
        if($id)
        {
            $user = User::with(['satker'])->find($id);

            if($user)
            {
                return ResponseFormatter::success(
                    $user,
                    'Data User Berhasil Diambil'
                );
            }
            else
            {
                return ResponseFormatter::error(
                    null,
                    'Data User Tidak Ada',
                    404
                );
            }
        }

        $user = User::with(['satker']);

        if($username)
        {
            $user->where('username','like','%'. $username . '%');
        }

        if($name)
        {
            $user->where('name','=', $name);
        }

        if($email)
        {
            $user->where('email','=', $email);
        }

        return ResponseFormatter::success(
            $user->paginate($limit),
            'Data User Berhasil Diambil'
        );
    }

    public function login(Request $request)
    {
        //validasi input
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            //cek kredensial
            $credentials = request(['username', 'password']);
            if(!Auth::attempt($credentials)) {
                return ResponseFormatter::error([
                    'message' => 'Unauthorized'
                ], 'Authentication Failed', 500);
            }

            //pengecekan hash tidak sesuai
            $user = User::where('username', $request->username)->first();
            if(!Hash::check($request->password, $user->password, [])){
                throw new \Exception('Invalid Credentials');
            }

            //jika berhasil maka login
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ], 'Authenticated');

        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong coy disini',
                'error' => $error
            ], 'Authentication Error', 500);
        }
    }

    public function register(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required','string','max:255'],
                'username' => ['required','string','unique:users','max:255'],
                'email' => ['required','string','email','max:255']
                // 'password' => $this->passwordRules()

            ]);

            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'satker_id' => $request->satker_id,
                'roles' => $request->roles,
                'password' => Hash::make($request->password),
            ]);

            $user = User::where('username', $request->username)->first();
            $tokenResult = $user->createToken('authToken')->plainTextToken;

            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ]);
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong cuyy',
                'error' => $error
            ], 'Register Gagal', 500);
        }
    }

    public function logout(Request $request)
    {
        try{
            $token = $request->user()->currentAccessToken()->delete();

            return ResponseFormatter::success($token, 'Token Revoked');
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ], 'Authentication Error', 500);
        }
    }

    public function updateProfile(Request $request)
    {
        try 
        {
            $data = $request->all();

            $user = Auth::user();
            $user->update($data);

            return ResponseFormatter::success($user, 'Profile Updated');
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ], 'Authentication Error', 500);
        }
    }
        
    // public function fetch(request $request)
    // {
    //     $user = User::query()
    //     return ResponseFormatter::success($request->user(),'Data Profile User berhasil diambil');
    // }

    

    public function updatePhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|image|max:2048'
        ]);

        if($validator->fails())
        {
            return ResponseFormatter::error(
                ['error' => $validator -> errors()],
                'Update Foto Gagal',
                401
            );
        }

        if($request->file('file'))
        {
            $file = $request->file->store('assets/user', 'public');

            // simpan foto ke database
            $user = Auth::user();
            $user->profile_photo_path = $file;
            $user->update();

            return ResponseFormatter::success([$file],'File Berhasil Disimpan');
        }
    }

}
