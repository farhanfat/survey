<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 5);
        $pertanyaan = $request->input('pertanyaan');
        $urutan = $request->input('urutan');
        
        if($id)
        {
            $pertanyaan = Pertanyaan::find($id);

            if($pertanyaan)
            {
                return ResponseFormatter::success(
                    $pertanyaan,
                    'Data Pertanyaan Berhasil Diambil'
                );
            }
            else
            {
                return ResponseFormatter::error(
                    null,
                    'Data Pertanyaan Tidak Ada',
                    404
                );
            }
        }

        $pertanyaan = Pertanyaan::query();

        // //error dibawah ini
        // if($pertanyaan)
        // {
        //     $pertanyaan->where('pertanyaan','like','%'. $pertanyaan . '%');
        // }

        if($urutan)
        {
            $pertanyaan->where('urutan','=', $urutan);
        }

        return ResponseFormatter::success(
            $pertanyaan->paginate($limit),
            'Data Pertanyaan Berhasil Diambil'
        );
    }

    public function tambah(Request $request)
    {
        try {
            $request->validate([
                'urutan' => ['required','integer','max:10'],
                'pertanyaan' => ['required','string','unique:pertanyaan','max:255'],
            ]);

            Pertanyaan::create([
                'pertanyaan' => $request->pertanyaan,
                'urutan' => $request->urutan,
            ]);
            $pertanyaan = pertanyaan::where('pertanyaan', $request->pertayaan)->first();
            return ResponseFormatter::success([
                'pertanyaan' => $pertanyaan
            ]);
        } catch(Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong cuyy',
                'error' => $error
            ], 'Tambah Data Gagal', 500);
        }
    }
}
