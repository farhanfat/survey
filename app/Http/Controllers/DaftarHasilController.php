<?php

namespace App\Http\Controllers;

use App\Models\DaftarHasil;
use App\Models\Satker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DaftarHasilController extends Controller
{
    public function index ()
    {
        return view('pages.daftarhasil.index', [
            'satker' => Satker::class
        ]);
    }

    public function detailHasil($idSatker){
        $data = array();
        $data['idSatker'] = $idSatker;
        return view('pages.daftarhasil.detail', $data);
    }

    public function hasil(){
        $data = array();
        $user = auth()->user();
        $data['idSatker'] = $user->satker_id;
        return view('pages.daftarhasil.detailsingle', $data);
    }

    public function getGraphic($idSatker) {
        $pertanyaan = DB::table('pertanyaans')
            ->select("id", "pertanyaan")
            ->orderBy('urutan', 'asc');
        $dataPertanyan = $pertanyaan->get();
        $date_start = $_POST['start_date']." 00:00:00";
        $date_end = $_POST['end_date']." 23:59:59";

        $result = array();
        $jawaban = array('sangat_kurang','kurang','cukup','baik','sangat_baik');
        $labelKawaban = array('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik');
        if(!empty($dataPertanyan)){
            foreach ($dataPertanyan as $key => $value) {
                $jawabs = DB::table('jawaban as jwb')
                    ->select(DB::raw('count(jwb_d.jawaban) as jawabs, jwb_d.jawaban, pty.pertanyaan'))
                    ->leftJoin('jawaban_detail as jwb_d', 'jwb_d.id_jawaban', '=', 'jwb.id')
                    ->leftJoin('pertanyaans as pty', 'pty.id', '=', 'jwb_d.pertanyaan_id')
                    ->where('jwb.satker_id', '=', $idSatker)
                    ->where('jwb_d.pertanyaan_id', '=', $value->id);
                if($date_start != ""){
                    $jawabs = $jawabs->where('jwb_d.created_at', '>=', $date_start);
                }
                if($date_end != ""){
                    $jawabs = $jawabs->where('jwb_d.created_at', '<=', $date_end);
                }
                $jawabs = $jawabs->groupBy('jwb_d.jawaban')
                    ->groupBy('pty.pertanyaan');
                $dataJawab = $jawabs->get();
                $result[$key]['pertanyaan'] = $value->pertanyaan;
                $result[$key]['label'] = $labelKawaban;

                $datas = array();
                foreach ($jawaban as $keyJawab => $valJawab) {
                    $datas[$keyJawab] = 0;
                    if(!empty($dataJawab)){
                        foreach($dataJawab as $valData){
                            if($valData->jawaban == $valJawab) {
                                $datas[$keyJawab] = $valData->jawabs;
                            }
                        }
                    }
                }
                $result[$key]['datas'] = $datas;
            }
        }
        echo json_encode($result);
    }

    public function getPieChart($idSatker){
        $date_start = $_POST['start_date'];
        $date_end = $_POST['end_date'];

        $jawabs = DB::table('jawaban as jwb')
        ->select(DB::raw('count(jwb_d.jawaban) as jawabs, jwb_d.jawaban'))
        ->leftJoin('jawaban_detail as jwb_d', 'jwb_d.id_jawaban', '=', 'jwb.id')
        ->where('jwb.satker_id', '=', $idSatker);
        if($date_start != ""){
            $jawabs = $jawabs->where('jwb_d.created_at', '>=', $date_start);
        }
        if($date_end != ""){
            $jawabs = $jawabs->where('jwb_d.created_at', '<=', $date_end);
        }
        
        $jawabs = $jawabs->groupBy('jwb_d.jawaban')
        ->groupBy('jwb.satker_id');
        $dataJawab = $jawabs->get();

        $result = array();
        $jawaban = array('sangat_kurang','kurang','cukup','baik','sangat_baik');
        $labelKawaban = array('Sangat Kurang','Kurang','Cukup','Baik','Sangat Baik');

        $datas = array();
        foreach ($jawaban as $keyJawab => $valJawab) {
            $datas[$keyJawab] = 0;
            if(!empty($dataJawab)){
                foreach($dataJawab as $valData){
                    if($valData->jawaban == $valJawab) {
                        $datas[$keyJawab] = $valData->jawabs;
                    }
                }
            }
        }

        $result['datas'] = $datas;
        $result['label'] = $labelKawaban;

        echo json_encode($result);
    }
}
