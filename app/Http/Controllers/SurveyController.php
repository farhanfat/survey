<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Jawaban;
use App\Models\JawabanDetail;

class SurveyController extends Controller
{
    public function index ($satker)
    {
        $search = str_replace("-"," ",$satker);
        $data = DB::table('satkers')
            ->select("id", "nama_satker")
            ->where('nama_satker', 'like', '%'.strtoupper($search).'%');
        $dataSatker = $data->first();
        $data = array();
        $data['satker'] = $dataSatker;
        $data['satker_url'] = $satker;
        return view('frontoffice.index', $data);
    }

    public function surveyWithName($satker, $responden)
    {
        $search = str_replace("-"," ",$satker);
        $data = DB::table('satkers')
            ->select("id", "nama_satker")
            ->where('nama_satker', 'like', '%'.strtoupper($search).'%');
        $dataSatker = $data->first();
        $data = array();
        $data['satker'] = $dataSatker;
        $data['nik_responden'] = $responden;
        $data['satker_url'] = $satker;
        return view('frontoffice.indexWithName', $data);
    }

    public function getPertanyaan() {
        $data = DB::table('pertanyaans')
            ->select("id", "pertanyaan")
            ->orderBy('urutan', 'asc');
        $dataSatker = $data->get();

        echo json_encode($dataSatker);
    }

    public function saveAnswer(Request $request){
        $nikResponden = $request->nik_responden;
        $idSakter = $request->id_satker;
        $dataAnswer = $request->data_answer;
        $urlSatker = $request->url_satker;

        $saveAnswer = new Jawaban;
        $saveAnswer->jawaban_dari = $nikResponden;
        $saveAnswer->satker_id = $idSakter;
        $result = $saveAnswer->save();
        $idAnswer = $saveAnswer->id;

        $setQuestion = array();
        if(!empty($dataAnswer)) {
            foreach($dataAnswer as $key => $val) {
                $saveDetailAnswer = new JawabanDetail;
                $saveDetailAnswer->id_jawaban = $idAnswer;
                $saveDetailAnswer->pertanyaan_id = $val['id'];
                $saveDetailAnswer->jawaban = $val['value'];
                $result = $saveDetailAnswer->save();

                $data = DB::table('pertanyaans')
                    ->select("pertanyaan")
                    ->where('id', '=', $val['id']);
                $dataQuestion = $data->first();

                $setQuestion[$key]['pertanyaan'] = $dataQuestion->pertanyaan;
                $setQuestion[$key]['jawaban'] = $val['text'];
            }
        }

        $token = env('KEJAKSAAN_TOKEN', '6400d8d96832a21178b875de6743eac7');
        $url = env('KEJAKSAAN_URL','https://bukutamu.kejaksaan.go.id/api/survey').'/'.$urlSatker.'/'.$nikResponden;

        $headers = array('Authorization: Bearer '.$token, 'Content-Type: application/json');

        $fields = array('payload' => $setQuestion);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        $response = json_decode($result);
        $setResponse = array();
        if(!empty($response->datas->unique_kunjungan)){
            $setUrl = env('KEJAKSAAN_URL_NIK','https://bukutamu.kejaksaan.go.id/kunjungan/in-out').'/'.$response->datas->unique_kunjungan;
            $setValid = true;
            // $msg = $response->message;
            $msg = 'Data anda berhasil disimpan, terimakasih telah melakukan survey';
        }else{
            $setUrl = env('APP_URL','http://localhost');
            $setValid = false;
            // $msg = $response->message;
            $msg = 'Terjadi Kesalahan';
        }
        $setResponse['response'] = $setValid;
        $setResponse['url'] = $setUrl;
        $setResponse['message'] = $msg;

        echo json_encode($setResponse);
    }
}
