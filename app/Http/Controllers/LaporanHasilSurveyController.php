<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LaporanHasilSurvey;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LaporanHasilSurveyExport;

class LaporanHasilSurveyController extends Controller
{
    public function index ()
    {
        return view('pages.laporanhasilsurvey.index', ['laporanhasilsurvey' => LaporanHasilSurvey::class]);
    }

    public function exportExcel(){
        $date = \Carbon\Carbon::now();
        $fileName = $date.'-Laporan-Hasil-Survey.xlsx';

        return Excel::download(new LaporanHasilSurveyExport(), $fileName);
    }
}
