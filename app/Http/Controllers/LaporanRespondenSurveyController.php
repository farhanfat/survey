<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\LaporanRespondenSurvey;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LaporanRespondenSurveyExport;

class LaporanRespondenSurveyController extends Controller
{
    public function index ()
    {
        return view('pages.laporanrespondensurvey.index', ['laporanrespondensurvey' => LaporanRespondenSurvey::class]);
    }

    public function exportExcel(){
        $date = \Carbon\Carbon::now();
        $fileName = $date.'-Laporan-Responden-Survey.xlsx';

        return Excel::download(new LaporanRespondenSurveyExport(), $fileName);
    }
}
