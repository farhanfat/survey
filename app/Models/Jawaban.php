<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jawaban extends Model
{
    use HasFactory, SoftDeletes;

    // protected $fillable = [
    //     'satker_id', 'jawaban_dari', 'pertanyaan_id', 'jawaban' 
    // ];


    // public static function search($query)
    // {
    //     return empty($query) ? static::query()
    //         : static::where('jawaban_dari', 'like', '%'.$query.'%')
    //             ->orWhere('satker_id', 'like', '%'.$query.'%')
    //             ->orWhere('pertanyaan_id', 'like', '%'.$query.'%')
    //             ->orWhere('jawaban', 'like', '%'.$query.'%');
    // }

    // new
    protected $table = 'jawaban';
    protected $fillable = [
        'jawaban_dari', 'satker_id'
    ];


    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('jawaban_dari', 'like', '%'.$query.'%')
                ->orWhere('satker_id', 'like', '%'.$query.'%');
    }
}
