<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LaporanRespondenSurvey extends Model
{
    use HasFactory;

    public static function search($query, $dateStart, $dateEnd)
    {
        $data = DB::table('jawaban as j')
        ->select(DB::raw('s.id, s.nama_satker as satker, count(j.satker_id) as jumlah'))
        ->rightJoin('satkers as s', 's.id', '=', 'j.satker_id')
        ->leftJoin('jawaban_detail as jd', 'j.id', '=', 'jd.id_jawaban');

        if(!empty($query)){
            $data->where('s.nama_satker', 'like', '%'.strtoupper($query).'%');
        }
        if(!empty($dateStart)){
            $dateStart = $dateStart." 00:00:00";
            $data->where('jd.created_at', '>=', $dateStart);
        }
        if(!empty($dateEnd)){
            $dateEnd = $dateEnd." 23:59:59";
            $data->where('jd.created_at', '<=', $dateEnd);
        }

        $data->groupBy('j.satker_id');
        $data->groupBy('s.nama_satker');
        $data->groupBy('s.id');
        $data->orderByDesc(DB::raw('COUNT(j.satker_id)'));
        $data->orderBy('s.id');

        return $data;
    }
}
