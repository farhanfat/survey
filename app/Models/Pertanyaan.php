<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pertanyaan extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'pertanyaan', 'urutan'
    ];


    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('pertanyaan', 'like', '%'.$query.'%')
                ->orWhere('urutan', 'like', '%'.$query.'%');
    }
}
