<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DaftarHasil extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'parent_satker', 'nama_satker', 'tipe_satker'
    ];


    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('nama_satker', 'like', '%'.$query.'%');
                // ->orWhere('parent_satker', 'like', '%'.$query.'%');
    }

    public function parent_satkers(){
        return $this->hasOne(Satker::class, 'id', 'parent_satker');
    }
}
