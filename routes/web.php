<?php

use App\Http\Controllers\DaftarHasilController;
use App\Http\Controllers\LaporanHasilSurveyController;
use App\Http\Controllers\LaporanRespondenSurveyController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\SatkerController as ControllersSatkerController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontoffice.home');
});

Route::get('/survey/{satker}', 'App\Http\Controllers\SurveyController@index');
Route::get('/survey/{satker}/{name_responden}', 'App\Http\Controllers\SurveyController@surveyWithName');

Route::group([ "middleware" => ['auth:sanctum', 'verified'] ], function() {
    Route::view('/dashboard', "dashboard")->name('dashboard');

    Route::get('/user', [ UserController::class, "index" ])->name('user');
    Route::view('/user/new', "pages.user.user-new")->name('user.new');
    Route::view('/user/edit/{userId}', "pages.user.user-edit")->name('user.edit');

    Route::get('/satker', [ ControllersSatkerController::class, "index" ])->name('satker');
    Route::view('/satker/new', "pages.satker.satker-new")->name('satker.new');
    Route::view('/satker/edit/{satkerId}', "pages.satker.satker-edit")->name('satker.edit');


    Route::get('/daftarhasil', [ DaftarHasilController::class, "index" ])->name('daftarhasil');
    Route::get('/daftarhasil/view/{satkerId}', 'App\Http\Controllers\DaftarHasilController@detailHasil');
    Route::get('/daftarhasil/view/', 'App\Http\Controllers\DaftarHasilController@hasil')->name('hasil');
    Route::post('/daftarhasil/view/data-grapich/{satkerId}', 'App\Http\Controllers\DaftarHasilController@getGraphic');
    Route::post('/daftarhasil/view/data-pieChart/{satkerId}', 'App\Http\Controllers\DaftarHasilController@getPieChart');

    Route::get('/pertanyaan', [ PertanyaanController::class, "index" ])->name('pertanyaan');
    Route::view('/pertanyaan/new', "pages.pertanyaan.pertanyaan-new")->name('pertanyaan.new');
    Route::view('/pertanyaan/edit/{pertanyaanId}', "pages.pertanyaan.pertanyaan-edit")->name('pertanyaan.edit');

    Route::get('/respondensurvey', [ LaporanRespondenSurveyController::class, "index" ])->name('respondensurvey');
    Route::get('/export_responden_survey', 'App\Http\Controllers\LaporanRespondenSurveyController@exportExcel')->name('respondensurvey.exportExcel');
    Route::get('/hasilsurvey', [ LaporanHasilSurveyController::class, "index" ])->name('hasilsurvey');
    Route::get('/export_hasil_survey', 'App\Http\Controllers\LaporanHasilSurveyController@exportExcel')->name('hasilsurvey.exportExcel');
});
