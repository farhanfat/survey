<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Survey Form</title>
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
  </head>
  <body>
    <div class="cs_container">
      <img src="/css/logo.png" alt="Kejaksaan RI" style="width:100px;height:100px;margin-bottom:30px">
      <h3>Survey Kepuasan Masyarakat</h3>
      <h4>Aplikasi ini diperuntukan sebagai akhir dari Pelayanan Terpadu Satu Pintu</h4>

      <div class="form-outer" style="padding-top:10%;">
        <form action="#">
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                  <label for="#">Nama Responden</label>
                  <input type="text" class="form-control" />
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-primary">Lanjutkan</button>
              </div>
            </div>
          </div>

          <div class="page">
            <div class="title">Pertanyaan ada disini</div>
            <div class="field">
              <img src="{{ asset('images/1.png') }}" alt="" style="width:15%;">
              <img src="{{ asset('images/2.png') }}" alt="" style="width:15%;">
              <img src="{{ asset('images/3.png') }}" alt="" style="width:15%;">
              <img src="{{ asset('images/4.png') }}" alt="" style="width:15%;">
              <img src="{{ asset('images/5.png') }}" alt="" style="width:15%;">
            <div class="field"></div>
          </div>
        </form>
      </div>
    </div>
    <!-- <script src="/js/script.js"></script> -->
    <script src="/js/jquery.min.js"></script>
    <!-- <script src="/js/jquery-3.5.1.slim.min.js"></script> -->
    <script src="/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
