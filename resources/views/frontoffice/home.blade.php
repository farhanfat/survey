<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Survey Form</title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="/css/logo.png">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
  </head>
  <body>
    <div class="cs_container">
      <img src="/css/logo.png" alt="Kejaksaan RI" style="width:100px;height:100px;margin-bottom:30px">
      <h3>Pelayanan Terpadu Satu Pintu</h3><br/>
      <h4>Aplikasi Survey Kepuasan Masyarakat</h4>

      <div class="form-outer" style="padding-top:10%;">

      </div>
    </div>
    <!-- <script src="/js/script.js"></script> -->
    <script src="/js/jquery.min.js"></script>
    <!-- <script src="/js/jquery-3.5.1.slim.min.js"></script> -->
    <script src="/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
