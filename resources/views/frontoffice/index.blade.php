<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Survey Kepuasan Masyarakat</title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="/css/logo.png">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .center_1 {
            margin: auto;
            padding: 10px;
        }
    </style>
</head>

<body>
    <div class="cs_container">
        <img src="/css/logo.png" alt="Kejaksaan RI" style="width:100px;height:100px;margin-bottom:30px">
        <h3>Survey Kepuasan Masyarakat</h3>
        <h4>{{$satker->nama_satker}}</h4>

        <div class="form-outer" style="padding-top:10%;">
            <form action="#" autocomplete="off">
                <div class="row" id="display_response" style="display: inline;">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="#">NIK Responden</label>
                            <input type="text" class="form-control" name="nik_responden" value="" />
                            <input type="hidden" name="id_satker" value="{{$satker->id}}">
                        </div>
                        <div class="form-group">
                        <button type="button" id="btn-input-name" class="btn btn-primary">Lanjutkan</button>
                        </div>
                    </div>
                </div>

                <div class="page" id="display_question" style="display: none;">
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-konfirmasi" tabindex="-1" role="dialog" aria-labelledby="#" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title m-0" id="#">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        X
                    </button>
                </div><!--end modal-header-->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p id="msg_confirm">Anda yakin ingin melanjutkan?</p>
                        </div>
                    </div><!--end row-->   
                </div><!--end modal-body-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Tidak</button>
                    <button type="button" class="btn btn-primary btn-sm" id="btn-modal-ya" onclick="" data-dismiss="modal">Ya</button>
                </div><!--end modal-footer-->
            </div><!--end modal-content-->
        </div><!--end modal-dialog-->
    </div>
    <!--end modal-->

    <div class="modal fade" id="modal-sukses" tabindex="-1" role="dialog" aria-labelledby="#" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="text-center">
                    <img src="/images/img-check.svg">
                    <h4>Terima kasih telah menjawab.</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" onclick="refreshPage()" data-dismiss="modal">OK</button>
                </div><!--end modal-footer-->
            </div><!--end modal-content-->
        </div><!--end modal-dialog-->
    </div><!--end modal-->

    <div class="modal fade" id="modal-gagal" tabindex="-1" role="dialog" aria-labelledby="#" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="text-center">
                    <img src="/images/img-error.png">
                    <h4 id="title-modal-gagal">Terima kasih telah menjawab.</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" onclick="refreshPage()" data-dismiss="modal">OK</button>
                </div><!--end modal-footer-->
            </div><!--end modal-content-->
        </div><!--end modal-dialog-->
    </div><!--end modal-->

    <div class="modal fade" id="modal-loading" tabindex="-1" role="dialog" aria-labelledby="#" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="center_1">
                    <div class="loader"></div>
                    <br />
                    <div class="text-center">
                        <h4 id="title-modal-gagal">Loading.......</h4>
                    </div>
                </div>
            </div><!--end modal-content-->
        </div><!--end modal-dialog-->
    </div><!--end modal-->

    <!-- <script src="/js/script.js"></script> -->
    <script src="/js/jquery.min.js"></script>
    <!-- <script src="/js/jquery-3.5.1.slim.min.js"></script> -->
    <script src="/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript">
        var noQuestion = 0;
        var dataQuestion = [];
        var totalDataQuestion = 0;

        $(document).ready(function() {
            $(`#btn-input-name`).click(function(){
                var name = $(`input[name=name_response]`).val();
                if (name == "") {
                    $(`#modal-konfirmasi`).modal('show')
                    $(`#msg_confirm`).css('color', 'red');
                    $(`#msg_confirm`).html('isi nama terlebih dahulu');
                    $(`#btn-modal-ya`).prop('disabled', true);
                }else{
                    $(`#modal-konfirmasi`).modal('show')
                    $(`#msg_confirm`).css('color', 'green');
                    $(`#msg_confirm`).html('Anda yakin ingin melanjutkan?');
                    $(`#btn-modal-ya`).prop('disabled', false);
                    $(`#btn-modal-ya`).attr('onclick', `actionNext('to_question', 0)`)
                }
            })
        });

        function actionNext(action, no_question) {
            switch(action){
                case "to_question":
                    loadQuestions();
                break;
                default:
                    console.log("no action")
                break;
            }
        }

        function loadQuestions() {
            $(`#display_response`).hide(500);
            $("#display_question").hide(500);
            $.ajax({
                url: "/api/question",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    var tmp = "";
                    $("#display_question").html("");
                    var count = 0;
                    $.each(data, function(index, val) {
                        var displays = "none";
                        if (index == 0) {
                            displays = "inline";
                        }
                        tmp = `
                        <div id="ctn_question_${index}" style="display: ${displays};">
                            <h4 style="margin-bottom: 10%;" id="tmp_question">${val.pertanyaan}</h4>
                            <div class="field" style="margin-bottom: 5%;">
                                <a href="javascript:void(0)" onclick="loadModalNextQuestion(${index}, ${val.id}, 'sangat_kurang', 'Sangat Kurang')" title="Sangat Kurang"><img src="{{ asset('images/1.png') }}" alt="" style="width:15%;"></a>
                                <a href="javascript:void(0)" onclick="loadModalNextQuestion(${index}, ${val.id}, 'kurang', 'Kurang')" title="Kurang"><img src="{{ asset('images/2.png') }}" alt="" style="width:15%;"></a>
                                <a href="javascript:void(0)" onclick="loadModalNextQuestion(${index}, ${val.id}, 'cukup', 'Cukup')" title="Cukup"><img src="{{ asset('images/3.png') }}" alt="" style="width:15%;"></a>
                                <a href="javascript:void(0)" onclick="loadModalNextQuestion(${index}, ${val.id}, 'baik', 'Baik')" title="Baik"><img src="{{ asset('images/4.png') }}" alt="" style="width:15%;"></a>
                                <a href="javascript:void(0)" onclick="loadModalNextQuestion(${index}, ${val.id}, 'sangat_baik', 'Sangat Baik')" title="Sangat Baik"><img src="{{ asset('images/5.png') }}" alt="" style="width:15%;"></a>
                            </div>
                        </div>
                        `;
                        $("#display_question").append(tmp);
                        count++;
                    })
                    totalDataQuestion = count;
                    $("#display_question").slideDown("slow");
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function loadModalNextQuestion(index, id, value, answer) {
            $(`#modal-konfirmasi`).modal('show')
            $(`#msg_confirm`).css('color', 'green');
            $(`#msg_confirm`).html(`Jawaban Anda '${answer}', ingin menyimpan jawabanya?`);
            $(`#btn-modal-ya`).prop('disabled', false);
            $(`#btn-modal-ya`).attr('onclick', `loadNextQuestion(${index}, ${id}, '${value}', '${answer}')`);
        }

        function loadNextQuestion(index, id, value, text) {
            var totalIndex = index+1;
            let datas = {};
            datas['id'] = id;
            datas["value"] = value;
            datas["text"] = text;
            dataQuestion.push(datas);
            
            if (totalDataQuestion == totalIndex){
                saveAnswer();
            }else{
                $(`#ctn_question_${index}`).hide(500);
                var nextQuestion = index+1;
                $(`#ctn_question_${nextQuestion}`).slideDown("slow");
            }
        }

        function saveAnswer() {
            let datas = {};
            datas["id_satker"] = $(`input[name=id_satker]`).val();
            datas["url_satker"] = '{{ $satker_url }}';
            datas["nik_responden"] = $(`input[name=nik_responden]`).val();
            datas["data_answer"] = dataQuestion;
            // console.log(datas)
            $(`#modal-loading`).modal('show');
            $.ajax({
                url: "/api/save-question",
                type: "POST",
                dataType: "json",
                data: datas,
                success: function(data) {
                    $('#modal-loading').modal('hide');
                    if(data.response){
                        location.assign(data.url);
                        location.replace(data.url);
                        // window.location.reload();
                        $(`#modal-sukses`).modal('show');
                    }else{
                        $(`#title-modal-gagal`).text(data.message);
                        $(`#modal-gagal`).modal('show');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function refreshPage(){
            window.location.href = "/";
        }
    </script>
</body>

</html>
