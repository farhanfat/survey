<div>
    <x-data-table :data="$data" :model="$laporanrespondensurvey">
        <x-slot name="head">
            <tr>
                <th><a wire:click.prevent="sortBy('satker')" role="button" href="#">
                    Nama Satker
                    @include('components.sort-icon', ['field' => 'satker'])
                </a></th>
                <th><a wire:click.prevent="sortBy('jumlah')" role="button" href="#">
                    Jumlah
                    @include('components.sort-icon', ['field' => 'jumlah'])
                </a></th>
            </tr>
        </x-slot>
        <x-slot name="body">
            <?php
                $count = 0;
            ?>
            @foreach ($laporanrespondensurvey as $val)
                <tr x-data="window.__controller.dataTableController({{ $val->id }})">
                    <td>{{ $val->satker }}</td>
                    <td>{{ $val->jumlah }}</td>
                </tr>
            @endforeach
        </x-slot>
    </x-data-table>
</div>
