<div>
    <x-data-table :data="$data" :model="$laporanhasilsurvey">
        <x-slot name="head">
            <tr>
                <th><a wire:click.prevent="sortBy('nama_satker')" role="button" href="#">
                    Nama Satker
                    @include('components.sort-icon', ['field' => 'nama_satker'])
                </a></th>
                <th><a wire:click.prevent="sortBy('sangat_kurang')" role="button" href="#">
                    Sangat Kurang
                    @include('components.sort-icon', ['field' => 'sangat_kurang'])
                </a></th>
                <th><a wire:click.prevent="sortBy('kurang')" role="button" href="#">
                    Kurang
                    @include('components.sort-icon', ['field' => 'kurang'])
                </a></th>
                <th><a wire:click.prevent="sortBy('cukup')" role="button" href="#">
                    Cukup
                    @include('components.sort-icon', ['field' => 'cukup'])
                </a></th>
                <th><a wire:click.prevent="sortBy('baik')" role="button" href="#">
                    Baik
                    @include('components.sort-icon', ['field' => 'baik'])
                </a></th>
                <th><a wire:click.prevent="sortBy('sangat_baik')" role="button" href="#">
                    Sangat Baik
                    @include('components.sort-icon', ['field' => 'sangat_baik'])
                </a></th>
            </tr>
        </x-slot>
        <x-slot name="body">
            <?php
                $count = 0;
            ?>
            @foreach ($laporanhasilsurvey as $val)
                <tr x-data="window.__controller.dataTableController({{ $val->id }})">
                    <td>{{ $val->nama_satker }}</td>
                    <td>{{ $val->sangat_baik }}</td>
                    <td>{{ $val->kurang }}</td>
                    <td>{{ $val->cukup }}</td>
                    <td>{{ $val->baik }}</td>
                    <td>{{ $val->sangat_baik }}</td>
                </tr>
            @endforeach
        </x-slot>
    </x-data-table>
</div>
