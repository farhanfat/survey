<div>
    <x-data-table :data="$data" :model="$satker">
        <x-slot name="head">
            <tr>
                <th>No</th>
                <th><a wire:click.prevent="sortBy('nama_satker')" role="button" href="#">
                    Nama Satker
                    @include('components.sort-icon', ['field' => 'nama_satker'])
                </a></th>
                <th><a wire:click.prevent="sortBy('parent_satker')" role="button" href="#">
                    Parent Satker
                    @include('components.sort-icon', ['field' => 'parent_satker'])
                </a></th>
                <th><a wire:click.prevent="sortBy('tipe_satker')" role="button" href="#">
                    Tipe Satker
                    @include('components.sort-icon', ['field' => 'tipe_satker'])
                </a></th>
                <th>Action</th>
            </tr>
        </x-slot>
        <x-slot name="body">
            <?php
                $count = 0;
            ?>
            @foreach ($satker as $satker)
                <tr x-data="window.__controller.dataTableController({{ $satker->id }})">
                    <?php $count++; ?>
                    <td>{{ $count }}</td>
                    <td>{{ $satker->nama_satker }}</td>
                    <td>{{ $satker->parent_satkers['nama_satker']}}</td>
                    <td>
                        @if (($satker->tipe_satker) == 1)
                        KEJAKSAAN AGUNG
                    @elseif (($satker->tipe_satker) == 2)
                        KEJAKSAAN TINGGI
                    @else
                        KEJAKSAAN NEGERI
                    @endif
                    </td>
                    <td class="whitespace-no-wrap row-action--icon">
                        <a role="button" href="/satker/edit/{{ $satker->id }}" class="mr-3"><i class="fa fa-16px fa-pen"></i></a>
                        <a role="button" x-on:click.prevent="deleteItem" href="#"><i class="fa fa-16px fa-trash text-red-500"></i></a>
                    </td>
                </tr>
            @endforeach
        </x-slot>
    </x-data-table>
</div>
