<div>
    <x-data-table :data="$data" :model="$pertanyaan">
        <x-slot name="head">
            <tr>
                <th><a wire:click.prevent="sortBy('id')" role="button" href="#">
                    NO
                </a></th>
                <th><a wire:click.prevent="sortBy('pertanyaan')" role="button" href="#">
                    Pertanyaan
                    @include('components.sort-icon', ['field' => 'pertanyaan'])
                </a></th>
                <th><a wire:click.prevent="sortBy('urutan')" role="button" href="#">
                    Urutan
                    @include('components.sort-icon', ['field' => 'urutan'])
                </a></th>
                <th><a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                    Tanggal Dibuat
                    @include('components.sort-icon', ['field' => 'created_at'])
                </a></th>
                <th>Action</th>
            </tr>
        </x-slot>
        <x-slot name="body">
            <?php
                $count = 0;
            ?>
            @foreach ($pertanyaan as $pertanyaan)
                <tr x-data="window.__controller.dataTableController({{ $pertanyaan->id }})">
                    <?php $count++; ?>
                    <td>{{ $count }}</td>
                    <td>{{ $pertanyaan->pertanyaan }}</td>
                    <td>{{ $pertanyaan->urutan }}</td>
                    <td>{{ $pertanyaan->created_at->format('d M Y H:i') }}</td>
                    <td class="whitespace-no-wrap row-action--icon">
                        <a role="button" href="/pertanyaan/edit/{{ $pertanyaan->id }}" class="mr-3"><i class="fa fa-16px fa-pen"></i></a>
                        <a role="button" x-on:click.prevent="deleteItem" href="#"><i class="fa fa-16px fa-trash text-red-500"></i></a>
                    </td>
                </tr>
            @endforeach
        </x-slot>
    </x-data-table>
</div>
