<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Data Satker') }}</h1>

        <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="#">Data Master</a></div>
            <div class="breadcrumb-item active"><a href="#">Data Satker</a></div>
            {{-- <div class="breadcrumb-item"><a href="{{ route('user') }}">Data User</a></div> --}}
        </div>
    </x-slot>

    <div>
        <livewire:table.main name="satker" :model="$satker" />
    </div>
</x-app-layout>
