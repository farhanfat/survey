<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Edit Satker') }}</h1>

        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item "><a href="#">Satker</a></div>
            <div class="breadcrumb-item Active"><a href="#">Edit Satker</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:create-satker action="updateSatker" :satkerId="request()->satkerId" />
    </div>
</x-app-layout>
