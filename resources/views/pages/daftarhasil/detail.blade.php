<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Daftar Hasil Survey') }}</h1>

        <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="#">Survey</a></div>
            <div class="breadcrumb-item active"><a href="#">Hasil Survey</a></div>
        </div>
    </x-slot>

    <!-- <div class="p-6 sm:px-20 bg-white border-b border-gray-200" style="margin-bottom: 2%">
        <div class="row">
            <div class="form-group col-span-6 sm:col-span-5">
                <x-jet-label for="name" value="{{ __('Nama') }}" />
                <input class="form-control" type="text" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-autoclose="true" name="star_date">
            </div>
        </div>
    </div> -->

    <div class="p-6 sm:px-20 bg-white border-b border-gray-200" style="margin-bottom: 2%">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group" style="margin-right: 15px;">
                    <x-jet-label for="name" class="form-control-label" value="{{ __('Tanggal Awal') }}" />
                    <input type="text" name="date_start" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-autoclose="true" value="{{date('Y-m-d')}}">
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group" style="margin-right: 15px;">
                    <x-jet-label for="name" value="{{ __('Tanggal Akhir') }}" />
                    <input type="text" name="date_end" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-autoclose="true" value="{{date('Y-m-d')}}">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>

            <div class="col-auto my-1" style="padding-top: 25px;">
                <button type="button" id="cari" class="btn btn-primary">Cari</button>
            </div>
        </div>
    </div>

   <div class="bg-white border-b border-gray-200 col-md-12 col-lg-12" style="padding: 30px;margin-bottom: 2%">
        <div id="cnt-pertanyaan" class="row">
        </div>
   </div>

   <div class="bg-white border-b border-gray-200 col-md-12 col-lg-12" style="padding: 30px;margin-bottom: 2%">
        <div class="row">
            <div class="col-md-12">
                <h2>Jawaban</h2>
                <canvas id="pie_chart"></canvas>
            </div>
        </div>
    </div>

</x-app-layout>

<script type="text/javascript">
    var barChart;
    var pieChart;
    $(document).ready(function() {
        $(`input[name=date_start]`).datepicker({
            dateFormat: "yy/mm/dd",
            orientation: "bottom" // add this for placemenet
        });

        $(`input[name=date_end]`).datepicker({
            dateFormat: "yy/mm/dd",
            orientation: "bottom" // add this for placemenet
        });
        getpieChart($("input[name=date_start]").val(), $("input[name=date_end]").val());
        getbarChart($("input[name=date_start]").val(), $("input[name=date_end]").val());

        $(`#cari`).click(function(){
            let search_date_start = $("input[name=date_start]").val();
            let search_date_end = $("input[name=date_end]").val();
            barChart.destroy();
            pieChart.destroy();

            getpieChart(search_date_start, search_date_end);
            getbarChart(search_date_start, search_date_end);
        })
    });

    function getbarChart(date_start, end_date){
        var idSatker = '{{$idSatker}}';
        var datas = {}
        datas['start_date'] = date_start;
        datas['end_date'] = end_date;

        $.ajax({
            url: "data-grapich/"+idSatker+'?_token=' + '{{ csrf_token() }}',
            type: "POST",
            dataType: "json",
            data: datas,
            success: function(response) {
                $(`#cnt-pertanyaan`).html('');
                $.each(response, function(i, val){
                    var data = {
                        labels: [],
                        datasets: []
                    }
                    var options = {
                        responsive: true,
                        legend: {
                            display: false
                        },
                    }
                    var tmp = `
                    <div class="col-md-6 col-lg-6" style="margin-bottom: 40px;">
                        <h2><b>${val.pertanyaan}</b></h2>
                        <br>
                        <canvas id="myChart_${i}"></canvas>
                    </div>
                    `
                    $(`#cnt-pertanyaan`).append(tmp);

                    let label = val.label;
                    let total = val.datas;
                    let background = [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 205, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(201, 203, 207, 1)'
                    ];
                    let borderColor = [
                    'rgb(255, 99, 132)',
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(75, 192, 192)',
                    'rgb(54, 162, 235)',
                    'rgb(153, 102, 255)',
                    'rgb(201, 203, 207)'
                    ]
                    let newDataset = new Array();
                    let setData = {}
                    setData['label'] = "Sembunyikan";
                    setData['data'] = total;
                    setData['backgroundColor'] = background;
                    setData['borderColor'] = borderColor;
                    newDataset.push(setData);
                    var json = {};
                    json.type = 'bar';

                    data.labels = label;
                    data.datasets = newDataset;

                    json.data = data;
                    json.options = options;
                    barChart = new Chart($(`#myChart_${i}`), json);
                })
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function getpieChart(date_start, end_date){
        var idSatker = '{{$idSatker}}';
        var datas = {}
        datas['start_date'] = date_start;
        datas['end_date'] = end_date;

        $.ajax({
            url: "data-pieChart/"+idSatker+'?_token=' + '{{ csrf_token() }}',
            type: "POST",
            dataType: "json",
            data: datas,
            success: function(response) {
                $(`#cnt-pertanyaan`).html('');
                var data = {
                    labels: [],
                    datasets: []
                }
                var options = {
                    responsive: true
                }

                let label = response.label;
                let total = response.datas;
                let background = [
                'rgba(255, 99, 132, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 205, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(201, 203, 207, 1)'
                ];
                let borderColor = [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
                ]
                let newDataset = new Array();
                let setData = {}
                setData['label'] = "Sangat Kurang";
                setData['data'] = total;
                setData['backgroundColor'] = background;
                setData['borderColor'] = borderColor;
                newDataset.push(setData);
                var json = {};
                json.type = 'pie';

                data.labels = label;
                data.datasets = newDataset;

                json.data = data;
                json.options = options;
                pieChart = new Chart($(`#pie_chart`), json);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }
</script>