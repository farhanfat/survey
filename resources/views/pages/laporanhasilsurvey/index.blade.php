<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Laporan Hasil Survey') }}</h1>

        <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="#">Laporan</a></div>
            <div class="breadcrumb-item active"><a href="#">Hasil Survey</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:table.main name="laporanhasilsurvey" :model="$laporanhasilsurvey" searchable="search_table, dateStart, dateEnd" />
    </div>

</x-app-layout>