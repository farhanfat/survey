<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Buat Pertanyaan Baru') }}</h1>

        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="#">Pertanyaan</a></div>
            <div class="breadcrumb-item active"><a href="#">Buat Pertanyaan Baru</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:create-pertanyaan action="createPertanyaan" />
    </div>
</x-app-layout>
