<x-app-layout>
    <x-slot name="header_content">
        <h1>{{ __('Edit Pertanyaan') }}</h1>

        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item "><a href="#">Pertanyaan</a></div>
            <div class="breadcrumb-item Active"><a href="#">Edit Pertanyaan</a></div>
        </div>
    </x-slot>

    <div>
        <livewire:create-pertanyaan action="updatePertanyaan" :pertanyaanId="request()->pertanyaanId" />
    </div>
</x-app-layout>
